import { useState } from "react";

let ReactClock = () =>{
  const [isShowTimer, setIsShowTimer] = useState(false);
  
  let onChange = () =>{
    setIsShowTimer(!isShowTimer);
  }

  return {
    onChange,
    isShowTimer
  };
}

export default ReactClock;
