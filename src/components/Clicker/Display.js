import React from 'react';
import './Clicker.css';
import 'antd/dist/antd.css';
import { Row, Col, Icon } from 'antd';
import UseClicker from './ClickerHook';

const display = () =>{
    const useHooks = UseClicker();

    return(
        <div>
            <Row className="clock-click">
                <Icon type="api" />
                &nbsp; React Clicker
            </Row>
            <div className="Clicker" >
                <span className="count">{useHooks.count}</span>
                <Row className="row">
                    <Col span={8} className="col"
                        onClick={useHooks.addCount}
                    >
                        <Icon type="plus" className="icon"/>
                    </Col>
                    <Col span={8} className="col"
                        onClick={useHooks.resCount}
                    >
                        <Icon type="reload"  className="icon"/>
                    </Col>
                    <Col span={8} className="col end"
                        onClick={useHooks.subCount}
                    >
                    <Icon type="minus" className="icon"/>
                    </Col>
                </Row>
            </div>
        </div> 
    );
}

export default display;