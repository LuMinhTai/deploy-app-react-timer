import React, { useState, useEffect } from "react";
import { Icon, Col, Row } from "antd";
import './Display.css';

let ReactTimer1 = () =>{
  const [seconds, setSeconds] = useState(0);
  const [isActive, setIsActive] = useState(false);

  let toggle = () =>{
    setIsActive(!isActive);
  }

  let reset = () =>{
    setSeconds(0);
    setIsActive(false);
  }

  useEffect(() => {
    let interval = null;
    if (isActive) {
      interval = setInterval(() => {
        setSeconds(seconds => seconds + 1);
      }, 1000);
    } else if (!isActive && seconds !== 0) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isActive, seconds]);

  return (
    <div>
      <Row className="center">
        <Col span={24} className="My-header">
          <Icon type="hourglass" />
          &nbsp; React Timer 1
        </Col>
      </Row>
      <div className="display">
        <div className="d-flex flex-column">
          <div className="text-info h6 align-self-center">
          </div>
          <h1>{seconds}</h1>
        </div>
        <div className="twoButton">
          <div>
            <div className="">
              <div className="d-flex flex-row">
                <div className="">
                  <div className="controls btn-group">
                    <button
                      onClick={toggle}
                      className="btn btn-lg btn-success"
                    >
                      {isActive ? (
                        <div className="text-rightpause">
                          <Icon type="pause-circle" />
                        </div>
                      ) : (
                        <div className="text-right">
                          <Icon type="caret-right" />
                        </div>
                      )}
                    </button>
                    <button className="btn btn-lg btn-primary" onClick={reset}>
                      <div className="text-left">
                        <Icon type="reload" />
                      </div>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ReactTimer1;
