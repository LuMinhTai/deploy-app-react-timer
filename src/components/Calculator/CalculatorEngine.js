const math = require('mathjs');


let currentValue = '';
    let register = [];
    let history = [];
    let result = '';
let CalculatorEngine = () =>{
    
    let inputDigit = (digit) =>{
        if (isNaN(digit)) {
            throw Error('Only numeric input is allowed');
        }

        if (result !== '') {
            result = '';
            currentValue = '';            
        }

        currentValue += digit;
    }

    let inputDecimal = () =>{
        if (result !== '') {
            result = '';
            currentValue = '';         
        }

        if (currentValue.indexOf('.') >= 0) {
            return;
        }

        if (currentValue === '') {
            currentValue += '0.';
        } else {
            currentValue += '.';
        }
    }

    let clear = () =>{
        currentValue = '';
        register = [];
        result = '';
    }

    let clearAll = () =>{
        currentValue = '';
        register = [];
        result = '';
        history = [];
    }

    let clearHistory = () =>{
        history = [];
    }

    let removeItem = () =>{
        if (currentValue === '') {
            return;
        }

        currentValue = currentValue.substring(0, currentValue.length - 1);
    }

    let add = () =>{
        if (currentValue === '') {
            return;
        }
        register.push(currentValue);
        register.push('+');

        currentValue = '';
        console.log(register)
    }

    let subtract = () =>{
        if (currentValue === '') {
            return;
        }

        register.push(currentValue);
        register.push('-');

        currentValue = '';
    }

    let multiply = () =>{
        if (currentValue === '') {
            return;
        }

        register.push(currentValue);
        register.push('*');

        currentValue = '';
    }

    let divide = () =>{
        if (currentValue === '') {
            return;
        }

        register.push(currentValue);
        register.push('/');
        
        currentValue = '';
    }

    let equals = () =>{
        if (currentValue === '') {
            return;
        }

        register.push(currentValue);

        const expression = register.join(' ');

        result = math.eval(expression);
        currentValue = result.toString();
        history.splice(0, 0, { expression, result });
        register = [];
    }

    let loadHistory = (index) =>{
        currentValue = history[index].result.toString();
    }

    let toggleSign = () =>{
        currentValue = (parseFloat(currentValue) * (-1)).toString();
    }

    let getValue = () =>{
        return currentValue;
    }

    let getExpression = () =>{
        return register.join(' ');
    }

    let getHistory = () =>{        
        return history;
    }

    let getResult = () =>{
        return result;
    }
    return {
        inputDigit,
        inputDecimal,
        getResult,
        getHistory,
        getExpression,
        getValue,
        toggleSign,
        loadHistory,
        equals,
        divide,
        multiply,
        subtract,
        add,
        clearHistory,
        clearAll,
        clear,
        removeItem
    }
}

export default CalculatorEngine;