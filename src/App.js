import React, { Component }from 'react';
import './App.css';
import Clock from './components/Clock/Display';
import Clicker from './components/Clicker/Display';
import ReactTimer1 from './components/Timer/React-Timer-1/ReactTimer1';
import ReactTimer2 from './components/Timer/React-Timer-2/ReactTimer2';
import ReactCalculator from './components/Calculator/Calculator';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import 'antd/dist/antd.css';
import { Icon,Menu,Row, Col  } from 'antd';
/***************************************** */
const { SubMenu } = Menu;

class App extends Component {
 
  render(){
    return (
        <Router>
            <div>
              <Row>
                <Col span={5}>
                  <Menu
                      defaultOpenKeys={['sub1']}
                      mode="inline"
                    >
                    <SubMenu
                      key="sub1"
                      title={
                        <span>
                        <Icon type="menu-unfold" />
                          <span className="menu-timer">MENU REACT TIMER</span>
                        </span>
                      }
                    >
                      <Menu.ItemGroup title="React Clicker" className='title-timer'>
                        <Menu.Item >
                            <Link to="/react-clicker">Clicker</Link>
                        </Menu.Item>
                      </Menu.ItemGroup>
                      <Menu.ItemGroup  title="React Clock" className='title-timer'>
                        <Menu.Item >
                            <Link to="/react-clock">Clock</Link>
                        </Menu.Item>
                      </Menu.ItemGroup>
                      <Menu.ItemGroup title="React Timer 1" className='title-timer'>
                        <Menu.Item >
                            <Link to="/react-timer-1">Timer 1</Link>
                        </Menu.Item>
                      </Menu.ItemGroup>
                      <Menu.ItemGroup  title="React Timer 2" className='title-timer'>
                        <Menu.Item >
                            <Link to="/react-timer-2">Timer 2</Link>
                        </Menu.Item>
                      </Menu.ItemGroup>
                      <Menu.ItemGroup  title="React Calculator" className='title-timer'>
                        <Menu.Item >
                            <Link to="/react-calculator">Calculator</Link>
                        </Menu.Item>
                      </Menu.ItemGroup>
                    </SubMenu>
                  </Menu> 
                </Col>
                <Col span={19}>
                  <div>
                      <Route path="/react-clicker" exact component={Clicker} />
                      <Route path="/react-clock" exact component={Clock} />
                      <Route path="/react-timer-1" exact component={ReactTimer1} />
                      <Route path="/react-timer-2" exact component={ReactTimer2} />
                      <Route path="/react-calculator" exact component={ReactCalculator} />
                  </div>
                </Col>
            </Row>
        </div>
      </Router>
    );
  }
}

export default App;
