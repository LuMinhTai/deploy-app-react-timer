
import React, { useState } from 'react';
import CalculatorEngine from './CalculatorEngine';
import { Row, Col, Icon} from "antd";
import './Calculator.css'
import 'antd/dist/antd.css';

let Calculator = () =>{
    const calculator = CalculatorEngine();
    const [expression, setexpression] = useState('');
    const [value, setvalue] = useState('');
    const [history, sethistory] = useState([]);
    const [showHistory, setshowHistory] = useState(false);
    
    let handleOnAdd = () =>{
        calculator.add();
        setexpression(() => {
            return calculator.getExpression();
        });
        setvalue(()=>{ return calculator.getValue().toString()});
    }

    
    let handleOnClear = () =>{
        calculator.clear();
        setexpression(()=>{return calculator.getExpression()});
        setvalue(()=>{return calculator.getValue().toString()});
    }

    
    let handleOnClearAll = () =>{
        calculator.clearAll();
        setexpression(()=>{return calculator.getExpression()});
        sethistory(()=>{return calculator.getExpression()});
        setvalue(()=>{return calculator.getValue().toString()});
    }
    
    let handleOnDecimalPoint = () =>{
        calculator.inputDecimal();
        setexpression(()=>{return calculator.getExpression()});
        setvalue(()=>{return calculator.getValue().toString()});
    }

    
    let handleOnDelete = () =>{
        calculator.removeItem();
        setvalue(()=>{return calculator.getValue().toString()});
    }

    
    let handleOnDigit = (number) =>{
        
        calculator.inputDigit(number);
        setvalue(()=>{return calculator.getValue()})
    }

    
    let handleOnDivide = () =>{
        calculator.divide();
        setexpression(()=>{return calculator.getExpression()});
        setvalue(()=>{return calculator.getValue().toString()});
    }

    
    let handleOnEquals = () =>{
        calculator.equals();
        setexpression(()=>{return calculator.getExpression()});
        setvalue(()=>{return calculator.getValue().toString()});
        sethistory(()=>{return calculator.getHistory()})
    }

    let handleOnMultiply = () =>{
        calculator.multiply();
        setexpression(()=>{return calculator.getExpression()});
        setvalue(()=>{return calculator.getValue().toString()})
    }

    
    let handleOnSubtract = () =>{
        calculator.subtract();
        setexpression(()=>{return calculator.getExpression()});
        setvalue(()=>{return calculator.getValue().toString()});
    }


    let handleOnToggleHistory = () =>{
        setshowHistory(prevState =>{
            return !prevState.showHistory
        })
    }

    
    let handleOnToggleSign = () =>{
        calculator.toggleSign();
        setvalue(()=>{return calculator.getValue().toString()})
    }

    let handleDigit = (e) => {
        handleOnDigit(e.target.value);
    };

    let removeHistory = (index) =>{
        console.log(index);
        history.splice(index,1);
        // sethistory(history)
        console.log(history)
    }

    let calShowHistory = () =>{
        let showHistory = history.map((expression, i) => {
            return (
                <div key={i}>
                    <Col span={8} className="cal-show-history-col-1">
                        <Icon type="delete" onClick={()=>removeHistory(i)}/>
                    </Col>
                    <Col span={13} className="cal-show-history-col-2">
                        <div className="text-right">
                                <div>{expression.expression}&nbsp;=</div>
                                <div className="font-weight-bold">{expression.result}</div>
                        </div>   
                    </Col>
                </div>
            );
        })    
        return showHistory;
    }
        return (
            <div className="calculator-container">
                <Row className="calculator">
                    <Icon type="calculator" />
                    &nbsp; React Calculator
                </Row>
               <Row className="cal-display">
                    <Col span={24} className="cal-display-1">  
                        <div>{expression}</div>
                    </Col>
                    <Col span={24}>  
                        <div>{value}</div>
                    </Col>
               </Row>   
                <Row className="cal-view-history">
                    <Col span={24}>  
                        <button 
                        disabled={!history.length > 0} 
                        onClick={handleOnToggleHistory}
                        className="view-history"
                        >
                            <Icon type="file-search" />
                        </button>
                    </Col>
                </Row>
                { 
                !showHistory && 
                <Row className="cal-button">
                      <Col span={24}>   
                            <button className="button"  onClick={handleOnClearAll}>CE</button>
                            <button className="button"  onClick={handleOnClear}>C</button>
                            <button className="button"  onClick={handleOnDelete}>
                                <Icon type="caret-right" />
                            </button>
                            <button className="button"  onClick={handleOnDivide}>&divide;</button>
                        </Col>
                        <Col span={24}>
                            <button className="button" value="7" onClick={handleDigit}>7</button>
                            <button className="button" value="8" onClick={handleDigit}>8</button>
                            <button className="button" value="9" onClick={handleDigit}>9</button>
                            <button className="button" value="*" onClick={handleOnMultiply}>&times;</button>
                        </Col>
                        <Col span={24}>
                            <button className="button" value="4" onClick={handleDigit}>4</button>
                            <button className="button" value="5" onClick={handleDigit}>5</button>
                            <button className="button" value="6" onClick={handleDigit}>6</button>
                            <button className="button" value="-" onClick={handleOnSubtract}>&minus;</button>
                        </Col>
                        <Col span={24}>
                            <button className="button" value="1" onClick={handleDigit}>1</button>
                            <button className="button" value="2" onClick={handleDigit}>2</button>
                            <button className="button" value="3" onClick={handleDigit}>3</button>
                            <button className="button" value="+" onClick={handleOnAdd}>&#43;</button>
                        </Col>
                        <Col span={24}>
                            <button className="button" value="+-" onClick={handleOnToggleSign}>&plusmn;</button>
                            <button className="button" value="0" onClick={handleDigit}>0</button>
                            <button className="button" value="." onClick={handleOnDecimalPoint}>.</button>
                            <button className="button" value="=" onClick={handleOnEquals}>=</button>
                        </Col>
                </Row>
                } 
                {
                showHistory && 
                <Row className="cal-show-history">
                    {calShowHistory()}
                </Row>       
                }
            </div>
        );
}

export default Calculator;