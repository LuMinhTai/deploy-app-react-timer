import React, { useState, useEffect } from "react";
import { Row, Col, Icon, Button, Input } from "antd";
import "./ReactTimer2.css";

let ReactTimer2 = () =>{
  const [isActive, setIsActive] = useState(true);
  const [time, setTime] = useState('0');
  const [hour, setHour] = useState(0);
  const [minute, setMinute] = useState(0); 
  const [second, setSecond] = useState(0);
  const [isRun, setIsRun] = useState(false);

  useEffect(()=>{
    let length = time.length;
    if(isRun) {
      const myInterval = setInterval(() => {
        if(second === 0) {
          if(minute === 0) {
            if(hour === 0) {
              clearInterval(myInterval);
            } else {
              setHour(hour - 1);
              setMinute(59);
              setSecond(59);
            }
          } else {
            setMinute(minute - 1);
            setSecond(59);
          }
        } else {
          setSecond(second - 1);
        }
      }, 1000);
      return () => clearInterval(myInterval);
    } else if (isActive){
      switch(length) {
        case 7:
        case 6:
          setHour(parseInt(time.slice(1,3)));
          setMinute(parseInt(time.slice(3,5)));
          setSecond(parseInt(time.slice(5)));
          break;
        case 5:
        case 4:
          setHour(parseInt(time.slice(1,3)));
          setMinute(parseInt(time.slice(3)));
          setSecond(0);
          break;
        case 3:
        case 2:
          setHour(parseInt(time.slice(1)));
          setMinute(0);
          setSecond(0);
          break;
        default:
          setHour(0);
          setMinute(0);
          setSecond(0);
      }
    }
  });

  let stop = () =>{
    setIsRun(false);
  }


  let start = () =>{
    setIsRun(true);
    setIsActive(!isActive);
  }

  let handleNumber = (param) =>{
    if(time.length < 7 || time === '') {
      let res = time + param;
      setTime(res);
    }
  }

  let onClear = () =>{
    let length = time.length;
    if(length !== 1) {
      setTime(time.slice(0, length - 1));
    }
  }

  let toggle = () =>{
    setIsActive(!isActive);
    setTime('0');
  }

  let mapButton = () =>{
    let arr = [1,2,3,4,5,6,7,8,9];
    let button = arr.map((item,index) =>{
      return(
        <Button 
          key={index} 
          onClick = {() => handleNumber(item)} 
          className="button"
        >{item}</Button>
      );
    });
    return button;
  }

  return (
    <div>
        <Row>
            <Col span={24} className="My-header head-react">
            <Icon type="hourglass" />
              &nbsp; React Timer 2
            </Col>
        </Row>
        <div className="caculartor">
        <Row>
          <Col span={12}>
              <Input placeholder="Input Hours:" value={ hour }  disabled className="input"/>
              <Input placeholder="Input Minutes:" value={ minute } disabled className="input"/>
              <Input placeholder="Input Seconds:" value={ second } disabled className="input"/>
          </Col>
        </Row>
        <Row>
            <Col span={12} className="calculatorButton">
              {mapButton()}
              <Button onClick = {() => onClear()} className="button">x</Button>
              <Button onClick = {() => handleNumber(0)} className="button">0</Button>
              <Button className="button" />
              {isActive ? (
                <Button onClick={start} className="startButton">
                  START
                </Button>
              ) : (
                <div className="Button">
                  {" "}
                  <Button onClick ={()=>stop()} className="stopButton">STOP</Button>{" "}
                  <Button onClick={toggle} className="resetButton">
                    RESET
                  </Button>
                </div>
              )}
            </Col>
        </Row>
        </div>
    </div>
  );
}

export default ReactTimer2;
