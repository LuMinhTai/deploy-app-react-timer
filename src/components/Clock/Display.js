import React from 'react';
import './DisplayClock.css'
import 'antd/dist/antd.css';
import Moment from "react-moment";
import { Row, Col, Icon } from 'antd';
import UseReactClock from './ClockHook';

const display = () =>{
    const useHooks = UseReactClock();
    
    return(
            <div className="span">
                <Row className="clock">
                    <Icon type="clock-circle" className="icon-clock" />
                    &nbsp; React Clock
                </Row>
                <div className="container-time">
                <Row>
                    <Col span={4} offset={10}>
                        <input type="checkbox" onChange={useHooks.onChange}/>
                        <span>On</span>
                        <span>Off</span>
                        <div></div>
                        <Icon type="calendar" className="icon-circle" />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h1 className="time">
                            <Moment interval={1000} format="HH:mm:ss"/>
                        </h1>
                        <div className="toggleClass">{useHooks.isShowTimer ? 
                            <p className="currentDate">
                                <Moment format="DD MMMM YYYY" className="toggle"/>
                            </p>
                         : null
                        }
                        </div>
                    </Col>
                </Row>
             </div>
        </div>  
    );
}

export default display;