import { useState } from 'react';

const useClicker = () =>{
    const [counter , setCounter] = useState(0);
    
    let addCount = () =>{
        setCounter(counter + 1);
        if(counter >= 30){
            setCounter(30);
        }
    }

    let subCount = () =>{
        setCounter(counter - 1);
        if(counter < 1){
            setCounter(0);
        }
    }

    let resCount = () =>{
        setCounter(0);
    }

    return {
        count: counter,
        addCount,
        subCount,
        resCount,
    };
}

export default useClicker;